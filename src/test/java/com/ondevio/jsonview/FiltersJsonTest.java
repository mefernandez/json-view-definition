package com.ondevio.jsonview;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ondevio.jsonview.data.Employee;
import com.ondevio.jsonview.data.NamesAndSalariesView;
import com.ondevio.jsonview.data.NamesOnlyView;
import com.ondevio.jsonview.data.Salary;

@RunWith(SpringRunner.class)
public class FiltersJsonTest {

	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Before
	public void setup() {
		mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}
	
	@Test
	public void testNamesOnlyView() throws Exception {
		// Data
		Employee mark = new Employee();
		mark.setName("Mark");
		Employee boss = new Employee();
		boss.setName("Bob");
		mark.setBoss(boss);

		// Setup
		PropertyFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
		FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);

		// Test
		String json = mapper.writer(filters).withView(NamesOnlyView.class).writeValueAsString(mark);

		// Assert
		assertThat(json, hasJsonPath("$.name", equalTo("Mark")));
		assertThat(json, hasJsonPath("$.boss.name", equalTo("Bob")));
	}

	@Test
	public void testNamesAndSalariesView() throws Exception {
		// Data
		Employee mark = new Employee();
		mark.setName("Mark");
		DateFormat dateInstance = new SimpleDateFormat("dd/MM/yyyy");
		dateInstance.setTimeZone(TimeZone.getTimeZone("GMT"));
		mark.addSalary(new Salary(new BigDecimal("1234.56"), dateInstance.parse("01/01/2017"), dateInstance.parse("31/07/2017")));
		Employee boss = new Employee();
		boss.setName("Bob");
		mark.setBoss(boss);

		// Setup
		PropertyFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
		FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);

		// Test
		String json = mapper.writer(filters).withView(NamesAndSalariesView.class).writeValueAsString(mark);

		// Assert
		assertThat(json, hasJsonPath("$.name", equalTo("Mark")));
		assertThat(json, hasJsonPath("$.salaries[0].amount", equalTo(1234.56)));
		assertThat(json, hasJsonPath("$.salaries[0].fromDate", equalTo("2017-01-01T00:00:00.000+0000")));
		assertThat(json, hasJsonPath("$.salaries[0].toDate", equalTo("2017-07-31T00:00:00.000+0000")));
		assertThat(json, hasJsonPath("$.boss.name", equalTo("Bob")));
		assertThat(json, not(hasJsonPath("$.boss.salaries")));

	}
	
	interface NoJsonDefinitionView {
		
	}

	@Test
	public void aViewWithoutJsonDefinitionGetsNothingSerialized() throws Exception {
		// Data
		Employee mark = new Employee();
		mark.setName("Mark");
		DateFormat dateInstance = new SimpleDateFormat("dd/MM/yyyy");
		dateInstance.setTimeZone(TimeZone.getTimeZone("GMT"));
		mark.addSalary(new Salary(new BigDecimal("1234.56"), dateInstance.parse("01/01/2017"), dateInstance.parse("31/07/2017")));
		Employee boss = new Employee();
		boss.setName("Bob");
		mark.setBoss(boss);

		// Setup
		PropertyFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
		FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);

		// Test
		String json = mapper.writer(filters).withView(NoJsonDefinitionView.class).writeValueAsString(mark);
		assertThat(json, equalTo("{}"));

	}
	
	@Test
	public void itResetsThePathUponException() throws Exception {
		// Data
		Employee mark = mock(Employee.class);
		when(mark.getName()).thenThrow(new RuntimeException("Thrown by FiltersJsonTest.itResetsThePathUponException"));

		// Setup
		JsonViewDefinitionFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
		FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);

		// Test
		try {
			mapper.writer(filters).withView(NamesOnlyView.class).writeValueAsString(mark);
		} catch (Throwable e) {
			// Assert
			assertThat(e.getMessage(), startsWith("Thrown by FiltersJsonTest.itResetsThePathUponException"));
			assertEquals("$", theFilter.path);
			return;
		}
		fail("RuntimeException was excepted but not thrown");
	}
}
