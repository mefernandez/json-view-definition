package com.ondevio.jsonview;

import static org.junit.Assert.*;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.ondevio.jsonview.JsonTrim;

public class JsonTrimTest {

	@Test
	public void testRemoveUnknownProperty() throws Exception {
		String jsonSpec = "{ \"name\": true }";
		String jsonData = "{ \"name\": \"Mark\", \"pass\": \"Secret\" }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("pass"));
	}

	@Test
	public void testRemoveUnknownNestedProperty() throws Exception {
		String jsonSpec = "{ \"name\": true }";
		String jsonData = "{ \"name\": \"Mark\", \"boss\": { \"name\": \"Bob\" } }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("boss"));
	}

	@Test
	public void testNestedArrayProperty() throws Exception {
		String jsonSpec = "{ \"name\": true, \"salaries\":  [ { \"fromDate\": true } ] }";
		String jsonData = "{ \"name\": \"Mark\", \"salaries\": [ { \"fromDate\": \"2017-01-01\", \"amount\": 100 } ] }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("salaries").get(0).get("amount"));
		assertEquals("2017-01-01", nodeData.get("salaries").get(0).get("fromDate").asText());
	}

	@Test
	public void testNestedObjectProperty() throws Exception {
		String jsonSpec = "{ \"name\": true, \"salary\":  { \"fromDate\": true } }";
		String jsonData = "{ \"name\": \"Mark\", \"salary\": { \"fromDate\": \"2017-01-01\", \"amount\": 100 } }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("salary").get("amount"));
		assertEquals("2017-01-01", nodeData.get("salary").get("fromDate").asText());
	}

	@Test
	public void itDoesNotThrowConcurrentModificationExceptionWhenRemovingPropertiesAtTheEndOfJsonData() throws Exception {
		// Tests no ConcurrentModificationException is thrown when iterating property "three" after removing "two". 
		String jsonSpec = "{\"one\":true}";
		String jsonData = "{\"one\":1,\"two\":2,\"three\":3}";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals(1, nodeData.get("one").asInt());
		assertNull(nodeData.get("two"));
		assertNull(nodeData.get("three"));
	}

	@Test
	public void itRemovesFirstElement() throws Exception {
		String jsonSpec = "{\"two\":true, \"three\":true}";
		String jsonData = "{\"one\":1,\"two\":2,\"three\":3}";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertNull(nodeData.get("one"));
		assertEquals(2, nodeData.get("two").asInt());
		assertEquals(3, nodeData.get("three").asInt());
	}

	@Test
	public void itRemovesSecondElement() throws Exception {
		String jsonSpec = "{\"one\":true, \"three\":true}";
		String jsonData = "{\"one\":1,\"two\":2,\"three\":3}";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals(1, nodeData.get("one").asInt());
		assertNull(nodeData.get("two"));
		assertEquals(3, nodeData.get("three").asInt());
	}
	
	@Test
	public void itRemovesSecondAndThirdElement() throws Exception {
		String jsonSpec = "{\"one\":true}";
		String jsonData = "{\"one\":1,\"two\":2,\"three\":3}";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals(1, nodeData.get("one").asInt());
		assertNull(nodeData.get("two"));
		assertNull(nodeData.get("three"));
	}
}
