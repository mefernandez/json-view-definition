package com.ondevio.jsonview.data;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.ondevio.jsonview.JsonViewDefinitionFilter;

import lombok.Data;

@Data
@JsonFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER)
public class Employee extends BaseEntity {
	
	private String name;

	// Sensitive!
	private String pass;

	// Sensitive
	private Set<Salary> salaries;
	
	private Employee boss;

	public void addSalary(Salary salary) {
		if (this.salaries == null) {
			this.salaries = new HashSet<>();
		}
		this.salaries.add(salary);
	}

}
