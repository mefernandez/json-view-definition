package com.ondevio.jsonview.data;

import com.fasterxml.jackson.annotation.JsonClassDescription;

@JsonClassDescription("classpath:com/example/json/experiments/filters/controller/NamesOnlyView.json")
public interface NamesOnlyView {
	
}
