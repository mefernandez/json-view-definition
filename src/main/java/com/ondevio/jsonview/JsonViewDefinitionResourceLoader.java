package com.ondevio.jsonview;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.annotation.JsonClassDescription;

import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
public class JsonViewDefinitionResourceLoader {
	
	private final ResourceLoader resourceLoader;
	
	public JsonViewDefinitionResourceLoader(ResourceLoader resourceLoader) {
		super();
		if (resourceLoader == null) {
			throw new IllegalArgumentException("A null ResourceLoader has been passed to JsonViewDefinitionResourceLoader constructor");
		}
		this.resourceLoader = resourceLoader;
	}

	@Getter
	private Map<Class<?>, String> cache = new HashMap<>();
	
	public String loadJsonViewDefinition(Class<?> activeView) throws IOException {
		String jsonDefinition = cache.get(activeView);
		if (jsonDefinition != null) {
			return jsonDefinition;
		}
		JsonClassDescription annotation = activeView.getAnnotation(JsonClassDescription.class);
		if (annotation == null) {
			return null;
		}
		String jsonResourcePath = annotation.value();
		try {
			Resource resource = resourceLoader.getResource(jsonResourcePath);
			jsonDefinition = StreamUtils.copyToString(resource.getInputStream(), Charset.forName("utf8"));
		} catch (IOException e) {
			log.error("Error trying lo load " + jsonResourcePath + " declared in View " + activeView);
			return null;
		}
		this.cache.put(activeView, jsonDefinition);
		return jsonDefinition;
	}

}
