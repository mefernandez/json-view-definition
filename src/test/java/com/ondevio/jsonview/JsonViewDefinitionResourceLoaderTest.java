package com.ondevio.jsonview;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.annotation.JsonClassDescription;
import com.ondevio.jsonview.JsonViewDefinitionResourceLoader;
import com.ondevio.jsonview.data.NamesOnlyView;

@RunWith(SpringRunner.class)
public class JsonViewDefinitionResourceLoaderTest {
	
	public interface TestView {
		
	}
	
	@JsonClassDescription("malformed-file-reference")
	public interface MalformedTestView {
		
	}

	@JsonClassDescription("classpath:path/to/non/existing/definition.json")
	public interface NonExistingTestView {
		
	}

	@Autowired
	private ResourceLoader resourceLoader;
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullResourceLoaderWillThrowException() {
		new JsonViewDefinitionResourceLoader(null);
	}
	
	@Test
	public void loadJsonViewDefinitionFromViewWithoutJsonClassDescriptionAnnotationReturnsNull() throws IOException {
		JsonViewDefinitionResourceLoader loader = new JsonViewDefinitionResourceLoader(resourceLoader);
		assertNull(loader.loadJsonViewDefinition(TestView.class));
	}
	
	@Test
	public void loadJsonViewDefinitionFromViewWithMalformedJsonFileReferenceReturnsNull() throws IOException {
		JsonViewDefinitionResourceLoader loader = new JsonViewDefinitionResourceLoader(resourceLoader);
		assertNull(loader.loadJsonViewDefinition(MalformedTestView.class));
	}
	
	@Test
	public void loadJsonViewDefinitionFromViewWithNonExistingJsonFileReferenceReturnsNull() throws IOException {
		JsonViewDefinitionResourceLoader loader = new JsonViewDefinitionResourceLoader(resourceLoader);
		assertNull(loader.loadJsonViewDefinition(NonExistingTestView.class));
	}
	
	@Test
	public void loadJsonViewDefinitionFromJsonResourceCachesResource() throws IOException {
		JsonViewDefinitionResourceLoader loader = new JsonViewDefinitionResourceLoader(resourceLoader);
		assertTrue(loader.getCache().isEmpty());
		String jsonViewDefinition = loader.loadJsonViewDefinition(NamesOnlyView.class);
		assertEquals(48, jsonViewDefinition.length());
		assertEquals(48, loader.getCache().get(NamesOnlyView.class).length());
	}

	@Test
	public void loadJsonViewDefinitionFromCache() throws IOException {
		JsonViewDefinitionResourceLoader loader = new JsonViewDefinitionResourceLoader(resourceLoader);
		assertTrue(loader.getCache().isEmpty());
		loader.getCache().put(TestView.class, "Just a test");
		String jsonViewDefinition = loader.loadJsonViewDefinition(TestView.class);
		assertEquals("Just a test", jsonViewDefinition);
	}
}
