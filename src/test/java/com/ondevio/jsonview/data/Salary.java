package com.ondevio.jsonview.data;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.ondevio.jsonview.JsonViewDefinitionFilter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER)
public class Salary extends BaseEntity {

	private BigDecimal amount;
	private Date fromDate;
	private Date toDate;
}
