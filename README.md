# JsonViewDefinition - A quick and convenient way to define @JsonView in JSON format

## The problem

Nowadays, it's easy to start a Spring Boot project with JPA 
and start creating `@Entity` class and writing `@Controllers`.

Just like:

```java
@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository repository;
	
	@Autowired
	private ObjectMapper mapper; 
	
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> findAll() {
		List<Employee> employees = repository.findAll();
		return ResponseEntity.ok(employees);
	}
}
```

```java
@Entity
public class Employee {
	
	private String name;

	// Sensitive!
	private String pass;

	// Sensitive
	@OneToMany
	private Set<Salary> salaries;
	
	@ManyToOne
	private Employee boss;

	public void addSalary(Salary salary) {
		if (this.salaries == null) {
			this.salaries = new HashSet<>();
		}
		this.salaries.add(salary);
	}

}

@Entity
public class Salary {

	private BigDecimal amount;
	private Date fromDate;
	private Date toDate;
}

```


**Before you even notice**, you'll be:

1. Fetching EAGERLY every model object.
2. Serializing/Deserializing your whole database.


Jackson provides @JsonView annotation to filter what gets de/serialized.
Just like:

```java
@Entity
public class Employee {

    @JsonView(SummaryView.class)	
	private String name;

	// Sensitive!
    @JsonView(DetailView.class)	
	private String pass;

	// Sensitive
    @JsonView(DetailView.class)	
	@OneToMany
	private Set<Salary> salaries;
	
    @JsonView(SummaryView.class)	
	@ManyToOne
	private Employee boss;

	public void addSalary(Salary salary) {
		if (this.salaries == null) {
			this.salaries = new HashSet<>();
		}
		this.salaries.add(salary);
	}

}
```

The problem is you end up with @JsonView annotations scattered across the whole application
and so you really loose control on what is getting de/serialized.

## The solution

This project lets you define a view in a JSON file and use it as the template to de/serialize objects.

Just like:

```json
{
	"name": true,
	"boss": {
		"name": true
	}
}
```

## Quick start


1. Add Maven dependency

```
<dependency>
  <groupId>com.ondevio</groupId>
  <artifactId>json-view-definition</artifactId>
  <version>0.0.1.RELEASE</version>
</dependency>
```

2. Add `JsonViewDefinitionFilter` to ObjectMapper.

```java
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer customizeObjectMapper(ResourceLoader resourceLoader) {
		return new Jackson2ObjectMapperBuilderCustomizer() {
			
			@Override
			public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
				jacksonObjectMapperBuilder.defaultViewInclusion(true);
				JsonViewDefinitionFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
				theFilter.setDefaultViewInclusion(true);
				FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);
				jacksonObjectMapperBuilder.filters(filters);
				jacksonObjectMapperBuilder.modules(new Hibernate5Module());
			}
		};
	}

}
```

3. Add `@JsonView` annotation to your `@Controller`.

```java
	@JsonView(NamesOnlyView.class)
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> findAll() {
		List<Employee> employees = repository.findAll();
		return ResponseEntity.ok(employees);
	}
```

4. Add `@JsonClassDescription` to the View marker interface pointing to the JSON file

```
@JsonClassDescription("classpath:com/example/json/experiments/filters/controller/NamesOnlyView.json")
public interface NamesOnlyView {
	
}
```

5. Create the JSON file in the aforementioned location listing the properties to be de/serialized.

```json
{
	"name": true,
	"boss": {
		"name": true
	}
}
```

6. Last but not least, add @JsonFiler annotation to the `@Entity` classes or use a MixIn.

```java
@JsonFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER)
@Entity
public class Employee {
   ...
}
```

-- OR --

```
@Bean
public Jackson2ObjectMapperBuilderCustomizer customizeObjectMapper(ResourceLoader resourceLoader) {
	return new Jackson2ObjectMapperBuilderCustomizer() {
		
		@Override
		public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
            ...
            jacksonObjectMapperBuilder.mixIn(Employee.class, JsonFilterMixIn.class);
            ...
        }
    }
}
   
@JsonFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER)
public interface JsonFilterMixIn {

}
```


## A working example

This repo hosts only the core classes.
Head on to https://gitlab.com/mefernandez/spring-jpa-jackson-experiments to see a Spring Boot application using it.

## About the JSON definition itself

The JSON format to define the view is not standard, like http://json-schema.org/.
But I've found this format to be a lot simpler.
Just pick a current JSON document and trim it down, replacing all values with `true`.
That's it.



